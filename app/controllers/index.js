/**
 * Module dependencies.
 */
var _ = require('lodash');
var db = require('../../config/sequelize');

exports.render = function(req, res) {
		if (req.query.fb_object_id) {
				var fbObjectId = "" + req.query.fb_object_id;
				db.Story.find({
						facebookObjectId: fbObjectId
				}).then(function(story){
						if (story) {
								res.redirect('/#/story/' + story.id);
						} else {
								res.redirect('/');
						}
				})
		} else {
				res.render('index', {
		        user: req.user ? JSON.stringify(req.user) : "null"
		    });
		}
};

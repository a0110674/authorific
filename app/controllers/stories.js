/**STILL PENDING STORY MODEL TO BE ADDED TO /app/models **/

/**
 * Module dependencies.
 */
var db = require('../../config/sequelize');
var logger = require('../../config/winston');
var graph = require('fbgraph');
var env = require('../../config/config')

/**
 * Find story by id
 * Note: This is called every time that the parameter :storyId is used in a URL. 
 * Its purpose is to preload the story on the req object then call the next function. 
 */
exports.story = function(req, res, next, id) {
    logger.info('storyId => ' + id);
    db.Story.find({
        where: {id: id},
        include: [db.User, {model: db.Post, include:[db.User]}, db.Like],
        order: [[db.Post, 'postNumber']]
    }).then(function(story){
        if(!story) {
            return res.status(404).send('Story not found');
        } else {
            req.story = story;
            return next();
        }
    }).catch(function(err){
        return next(err);
    });
};

/**
 * Find post by postNumber and storyId
 * Note: This is called every time that the parameter :postNumber is used in a URL. 
 * Its purpose is to preload the story on the req object then call the next function. 
 */
exports.post = function(req, res, next, postNumber) {
    logger.info('postNumber => ' + postNumber);
    db.Post.find({
        where: {
            StoryId: req.story.id,
            postNumber: postNumber
        },
        include: [db.User, db.Story]
    }).then(function(post){
        if(!post) {
            return res.status(404).send('Post not found');
        } else {
            req.post = post;
            return next();
        }
    }).catch(function(err){
        return next(err);
    });
};

function finishOngoingPostOnFBPage (story) {
    db.Story.find({
        where: {id: story.id},
        include: [db.Post],
        order: [[db.Post, 'postNumber']]
    }).then(function(story){
        graph.setAppSecret(null);
        var allContent = "";
        for (var i = 0; i < story.Posts.length; i++) {
            allContent += story.Posts[i].content + " ";
        }
        graph.post('/' + story.facebookPostId, {
            access_token: env.facebook.pageToken,
            message: "COMPLETED: " + story.title + "\n\n" + allContent + "\n\n" + "View this story at http://authorific.tk/#/story/" + story.id
        }, function(error, facebookResponse) {
            if (error) {
                logger.error(error);
            } else {
                logger.info(facebookResponse);
            }
        });
    });
}

function createPostOnFBPage (story, post) {
    graph.setAppSecret(null);
    graph.post('https://graph.facebook.com/'+env.facebook.pageID+'/feed',
    {
        access_token: env.facebook.pageToken,
        message: "ONGOING: " + story.title + "\n\n" + "Contribute at http://authorific.tk/#/story/" + story.id
    },function(err, response){
        if(err){
            logger.error(err);
        }
        else{
            logger.info(response);
            story.updateAttributes({facebookPostId: response.id});
        }
    });
}

function createFBObject (story, post) {
    graph.get('/oauth/access_token', {
        client_id: env.facebook.clientID,
        client_secret: env.facebook.clientSecret,
        grant_type: 'client_credentials'
    }, function(err, res){
        if (err) {
            logger.error(err);
        } else {
            logger.info(res);
            var appToken = res.access_token;
            json_post = JSON.stringify({
                og: {
                    title: story.title,
                    url: 'http://authorific.tk/#/story/' + story.id,
                    description: post.content + "..."
                }
            });
            graph.setAppSecret(env.facebook.clientSecret);
            graph.post('https://graph.facebook.com/app/objects/authorific:story',
                {
                    access_token: appToken,
                    object: json_post
                }, function(err, response){
                    if(err){
                        logger.error(err);
                        return new Error(err);
                    }
                    else{
                        logger.info(response);
                        story.updateAttributes({facebookObjectId: response.id});
                    }
                }
            );
        }
    });
}

/**
 * Create a story
 */
exports.createStory = function(req, res) {
    // augment the story by adding the UserId
    req.body.UserId = req.user.id;
    req.body.isPublic = true;
    req.body.isPublished = false;
    req.body.isFinished = false;
    req.body.title = req.body.title.trim();
    req.body.firstPost = req.body.firstPost.trim();
    // save and return and instance of story on the res object. 
    db.Story.create(req.body).then(function(story){
        if(!story){
            return res.send('users/signup', {errors: err});
        } else {
            if (!story.title) {
                story.updateAttributes({title: 'Story #' + story.id});
            }
            post = {content: req.body.firstPost, postNumber: 1, isAnonymous: story.isAnonymous, UserId: story.UserId, StoryId: story.id};
            db.Post.create(post).then(function(post){
                if (!post) {
                    return res.send('users/signup', {errors: err});
                } else {
                    createPostOnFBPage(story, post);
                    createFBObject(story, post);
                    return res.jsonp(story);
                }
            });
        }
    }).catch(function(err){
        return res.send('users/signup', { 
            errors: err,
            status: 500
        });
    });
};


/**
 * Create a post
 */
exports.createPost = function(req, res) {
    // augment the post by adding the UserId
    req.body.UserId = req.user.id;
    req.body.StoryId = req.story.id;
    req.body.content = req.body.content.trim();
    // save and return and instance of post on the res object. 
    db.Post.create(req.body).then(function(post){
        if(!post){
            return res.send('users/signup', {errors: err});
        } else {
            // if the postnumber of this new post is the last possible, mark the story as finished
            if (post.postNumber >= req.story.maxPost) {
                req.story.updateAttributes({
                    isFinished: true,
                    finishedAt: new Date()
                }).then(function(s){
                    finishOngoingPostOnFBPage(s);
                    return res.jsonp(post);
                });
            } else {
                return res.jsonp(post);
            }
        }
    }).catch(function(err){
        return res.send('users/signup', { 
            errors: err,
            status: 500
        });
    });
};

/**
 * Marks a story as finished
 */
exports.markStoryFinished = function(req, res) {
    // create a new variable to hold the story that was placed on the req object.
    var story = req.story;
    story.updateAttributes({
        isFinished: true,
        finishedAt: new Date()
    }).then(function(s){
        finishOngoingPostOnFBPage(s);
        return res.jsonp(s);
    }).catch(function(err){
        return res.render('error', {
            error: err, 
            status: 500
        });
    });
};

/**
 * Update a post
 */
exports.updatePost = function(req, res) {
    // create a new variable to hold the post that was placed on the req object.
    var post = req.post;
    if (req.story.isFinished) { // cannot edit posts in a finished story
        return res.render('error', {
            error: err, 
            status: 500
        });
    }
    post.updateAttributes({
        content: req.body.content.trim()
    }).then(function(p){
        return res.jsonp(p);
    }).catch(function(err){
        return res.render('error', {
            error: err, 
            status: 500
        });
    });
};

/**
 * Delete a story
 */
exports.destroyStory = function(req, res) {

    // create a new variable to hold the story that was placed on the req object.
    var story = req.story;

    story.destroy().then(function(){
        return res.jsonp(story);
    }).catch(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

/**
 * Delete a post
 */
exports.destroyPost = function(req, res) {
    // create a new variable to hold the post that was placed on the req object.
    var post = req.post;

    post.destroy().then(function(){
        return res.jsonp(post);
    }).catch(function(err){
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

/**
 * Show a story
 */
exports.showStory = function(req, res) {
    // Sending down the story that was just preloaded by the stories.story function
    // and saves story on the req object.\
    var story = req.story.get({plain: true});
    if (story.isAnonymous && (!req.user || story.UserId !== req.user.id)) {
        story.UserId = null;
        story.User = null;
    }
    for (var i = 0; i < story.Posts.length; i++) {
        if (story.Posts[i].isAnonymous && (!req.user || story.Posts[i].UserId !== req.user.id)) {
            story.Posts[i].UserId = null;
            story.Posts[i].User = null;
        }
    }

    // set like vars
    story.isLiked = false;
    if (req.user) {
        for (var i = 0; i < story.Likes.length; i++) {
            if (story.Likes[i].UserId === req.user.id) {
                story.isLiked = true;
                break;
            }
        }
    }
    story.Likes = null;
    return res.jsonp(story);
};

/**
 * List of Stories
 */
exports.getStories = function(req, res, next) {
    var query = req.query;
    // if query is empty 
    query = query ? query : {type:"popular", offset:0, limit:15};
    query.type = typeof query.type === "undefined" ? "popular" : query.type;
    query.offset = typeof query.offset === "undefined" ? 0 : query.offset;
    query.limit = typeof query.limit === "undefined" ? 15 : query.limit;
    query.isFinished = query.type !== "ongoing";
    var dbQuery = {};
    dbQuery.where = {isFinished: query.type !== "ongoing"};
    dbQuery.offset = query.offset;
    dbQuery.limit = query.limit;
    dbQuery.include = [db.User, {model: db.Post, include:[db.User]}, db.Like];
    dbQuery.order = [];
    if (query.type === "popular" || query.type === "trending") {
        dbQuery.order[dbQuery.order.length] = ['likeCount', 'DESC'];
    } else if (query.type === "new") {
        dbQuery.order[dbQuery.order.length] = ['finishedAt', 'DESC'];        
    } else if (query.type === "ongoing") {
        dbQuery.order[dbQuery.order.length] = ['createdAt', 'DESC'];
    }
    if (query.type === "trending") {
        dbQuery.where.finishedAt = {$gt: new Date(Date.now() - 3 * 86400000)}
    }

    dbQuery.order[dbQuery.order.length] = [db.Post, 'postNumber'];
    db.Story.findAll(dbQuery).then(function(stories){
        req.stories = stories;
        return next();
    }).catch(function(err){
        console.log(err);
        return res.render('error', {
            error: err,
            status: 500
        });
    });
};

/**
 * Like/Unlike a story
 */
exports.likeStory = function(req, res) {
    db.Like.findOrCreate({where: {UserId: req.user.id, StoryId: req.story.id}}).then(function(like) {
        var created = like[1];
        if (created) {
            req.story.updateAttributes({likeCount: req.story.likeCount + 1});
            var response = {likeCount: req.story.likeCount, isLiked: true};
            graph.setAccessToken(req.user.accessToken);
            graph.post('https://graph.facebook.com/' + req.story.facebookPostId + '/likes', function(err, facebookResponse1){
                if (err) {
                    logger.error(err);
                } else {
                    logger.info(facebookResponse1);
                    graph.get('https://graph.facebook.com/'+req.story.facebookPostId+'/likes', {summary: true}, function(err, facebookResponse){
                        if (err) {
                            logger.error(err);
                        } else {
                            logger.info(facebookResponse);
                            req.story.updateAttributes({likeCount: facebookResponse.summary.total_count});                            
                        }
                    });
                }
            });
            return res.jsonp(response);
        } else {
            like[0].destroy().then(function() {
                req.story.updateAttributes({likeCount: req.story.likeCount - 1});
                var response = {likeCount: req.story.likeCount, isLiked: false};
                graph.setAccessToken(req.user.accessToken);
                graph.del('https://graph.facebook.com/' + req.story.facebookPostId + '/likes', function(err,facebookResponse1){
                    if (err) {
                        logger.error(err);
                    } else {
                        logger.info(facebookResponse1);
                        graph.get('https://graph.facebook.com/' + req.story.facebookPostId + '/likes', {summary: true}, function(err, facebookResponse){
                            if (err) {
                                logger.error(err);
                            } else {
                                logger.info(facebookResponse);
                                req.story.updateAttributes({likeCount: facebookResponse.summary.total_count});
                            }
                        });
                    }
                });
                return res.jsonp(response);
            });  
        }
    })
    .catch(function(err){
        console.log(err);
        return res.render('error', {
            error: err,
            status: 500
        });
    });
}

exports.getFacebookComments = function(req, res){
    graph.setAccessToken(env.facebook.pageToken);
    graph.get(req.story.facebookPostId+'/comments', {
        summary: true,
        filter: 'toplevel'
    }, function(err,facebookResponse){
        if (err){
            logger.error(err);
            return res.status(500).send(JSON.stringify(err));
        }
        else{
            logger.info(facebookResponse);
            return res.jsonp(facebookResponse);
        }
    });
};

exports.share = function(req, res){
    graph.setAccessToken(req.user.accessToken);
    req.body.message = req.body.message ? req.body.message : "";
    shared = {
        story: req.story.facebookObjectId,
        message: req.body.message
    };
    graph.post('https://graph.facebook.com/me/authorific:recommend', shared, function(err, facebookResponse){
        if (err) {
            logger.error(err)
            return new Error(err);
        } else {
            logger.info(facebookResponse);
            return res.jsonp(facebookResponse);            
        }
    });
}

/**
 * Story authorizations routing middleware
 */
exports.hasStoryAuthorization = function(req, res, next) {
    if (req.story.User.id != req.user.id) {
      return res.status(401).send('User is not authorized');
    }
    next();
};

/**
 * Post authorizations routing middleware
 */
exports.hasPostAuthorization = function(req, res, next) {
    if (req.post.User.id != req.user.id) {
      return res.status(401).send('User is not authorized');
    }
    next();
};

/**
 * Checks if post is the last post in the story
 */
exports.isLastPost = function(req, res, next) {
    if (req.post.postNumber != req.story.Posts.length) {
      return res.status(409).send('Unable to modify post as it is not last post');
    }
    next();
};

/**
 * Checks if the last post in the story is the story's original post
 */
 exports.isNotOriginalPost = function(req, res, next) {
    if (req.postNumber === 1) {
        return res.status(401).send('Unable to modify post as it is not last post');
    }
    next();
 }


/**
 * Checks if the last post in the story was the user's own
 */
exports.isPrevPostNotOwn = function(req, res, next) {
    var numPosts = req.story.Posts.length;
    if (numPosts !== 0 && req.story.Posts[numPosts - 1].UserId === req.user.id) {
      return res.status(401).send('Unable to add new post as user owns the last post');
    }
    next();
};

/**
 * Filter anonymous post's userdata
 */
exports.filterData = function(req, res) {
    var stories = JSON.parse(JSON.stringify(req.stories));
    for (var i = 0; i < stories.length; i++) {
        if (stories[i].isAnonymous && (!req.user || stories[i].UserId !== req.user.id)) {
            stories[i].UserId = null;
            stories[i].User = null;
        }
        for (var j = 0; j < stories[i].Posts.length; j++) {
            if (stories[i].Posts[j].isAnonymous && (!req.user || stories[i].Posts[j].UserId !== req.user.id)) {
                stories[i].Posts[j].UserId = null;
                stories[i].Posts[j].User = null;
            }
        }
        // set like vars
        stories[i].isLiked = false;
        if (req.user) {
            for (var j = 0; j < stories[i].Likes.length; j++) {
                if (stories[i].Likes[j].UserId === req.user.id) {
                    stories[i].isLiked = true;
                    break;
                }
            }
        }
        stories[i].Likes = null;
    }
    return res.jsonp(stories);
};

/**
 * Deal with 304
 */
 exports.removeNotModified = function(req, res, next) {
    res.header("Cache-Control", "no-cache, no-store, must-revalidate");
    res.header("Pragma", "no-cache");
    res.header("Expires", 0);

    return next();
 }

/**
 * Checks if story is still unfinished (proceeds if story is unfinished)
 * Used for creating, editing and deleting posts
 */
exports.isStoryUnfinished = function(req, res, next) {
    if (req.story.isFinished) {
        return res.status(418).send('Story has been marked as finished');
    }
    return next();
}

/**
 * Checks if new post number is valid (for creating posts)
 */
exports.isNewPostNumberValid = function(req, res, next) {
    // cannot add new post if the postnumber is not the total number of posts + 1
    if (req.body.postNumber != req.story.Posts.length + 1) {
        return res.status(409).send("No longer last post");
    }
    return next();
}

/**
 * Validate story
 */
exports.validateStory = function(req, res, next) {
    var validateTitle = typeof req.body.title === "string" && req.body.title.trim().length > 0 && req.body.title.trim().length <= 50;
    var validateFirstPost = typeof req.body.firstPost === "string" && req.body.firstPost.trim().length > 0 && req.body.firstPost.trim().length <= 180;
    var validateAnonymity = typeof req.body.isAnonymous === "boolean";
    var validateMaxPost = typeof req.body.maxPost === "number" && req.body.maxPost > 0 && req.body.maxPost <= 50;
    if (validateTitle && validateFirstPost && validateAnonymity && validateMaxPost) {
        return next();
    } else {
        return res.status(500).send('Invalid story schema');
    }
}

/**
 * Validate post
 */
exports.validatePost = function(req, res, next) {
    var validateContent = typeof req.body.content === "string" && req.body.content.trim().length > 0 && req.body.content.trim().length <= 180;
    var validatePostNumber = typeof req.body.postNumber === "number" && req.body.postNumber > 0 && req.body.postNumber <= req.story.maxPost;
    var validateAnonymity = typeof req.body.isAnonymous === "boolean";
    if (validateContent && validatePostNumber && validateAnonymity) {
        return next();
    } else {
        return res.status(500).send('Invalid post schema');
    }
}
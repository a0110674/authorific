/**
 * Module dependencies.
 */
var db = require('../../config/sequelize');
var graph = require('fbgraph');
var env = require('../../config/config');
var logger = require('../../config/winston');

/**
 * Auth callback
 */
exports.authCallback = function(req, res, next) {
    res.redirect('/');
};

/**
 * Show login form
 */
exports.signin = function(req, res) {
    res.render('users/signin', {
        title: 'Signin',
        message: req.flash('error')
    });
};

/**
 * Show sign up form
 */
exports.signup = function(req, res) {
    res.render('users/signup', {
        title: 'Sign up',
    });
};

/**
 * Logout
 */
exports.signout = function(req, res) {
    //req.logout();
    //res.redirect('/');
    req.session.destroy(function (err) {
      if (err) {
        res.status(500).send('something happened');
      } else {
        res.status(200).send('successfully signed out');
      }
    });
};

/**
 * Session
 */
exports.session = function(req, res) {
    res.redirect('/');
};

/**
 * Create user
 */
exports.create = function(req, res) {
    var message = null;

    var user = db.User.build(req.body);

    user.provider = 'local';
    user.salt = user.makeSalt();
    user.hashedPassword = user.encryptPassword(req.body.password, user.salt);
    logger.info('New User (local) : { id: ' + user.id + ' username: ' + user.username + ' }');
    
    user.save().then(function(){
      req.login(user, function(err){
        if(err) return next(err);
        res.redirect('/');
      });
    }).catch(function(err){
      res.render('users/signup',{
          message: message,
          user: user
      });
    });
};

/**
 * Send User
 */
exports.me = function(req, res) {
    res.jsonp(req.user || null);
};

/**
 * Find user by id
 */
exports.user = function(req, res, next, id) {
    req.facebookUserId = id;
    next();
};

/**
 * Get Facebook data of a user
 */
exports.getFacebookData = function(req, res){
  graph.setAccessToken(env.facebook.pageToken);
  graph.get('https://graph.facebook.com/'+req.facebookUserId+'?fields=id,name,picture', function(err,facebook_reponse){
    if (err) {
      res.status(500).send(JSON.stringify(err));
    } else {
      res.jsonp(facebook_reponse);      
    }
  });
};

/**
 * Generic require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if (req.profile.id != req.user.id) {
      return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(sequelize, DataTypes) {

  var Like = sequelize.define('Like', {
    },
    {
      associate: function(models){
        Like.belongsTo(models.User);
        Like.belongsTo(models.Story);
      }
    }
  );

  return Like;
};

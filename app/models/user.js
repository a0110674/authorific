
/**
  * User Model
  */

var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {

  var User = sequelize.define('User',
    {
      facebookUserId: DataTypes.STRING,
      accessToken: DataTypes.STRING
    },
    {
      associate: function(models) {
        User.hasMany(models.Story);
        User.hasMany(models.Post);
        User.hasMany(models.Like);
      }
    }
  );

  return User;
};

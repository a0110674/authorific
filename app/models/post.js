

module.exports = function(sequelize, DataTypes) {

  var Post = sequelize.define('Post', {
      content: DataTypes.TEXT,
      postNumber: DataTypes.INTEGER,
      isAnonymous: DataTypes.BOOLEAN
    },
    {
      associate: function(models){
        Post.belongsTo(models.User);
        Post.belongsTo(models.Story);
      }
    }
  );

  return Post;
};

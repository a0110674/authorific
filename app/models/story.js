module.exports = function(sequelize, DataTypes) {

  var Story = sequelize.define('Story', {
      title: DataTypes.STRING,
      facebookObjectId: DataTypes.STRING,
      facebookPostId: DataTypes.STRING,
      maxPost: DataTypes.INTEGER,
      imageUrl: DataTypes.STRING,
      isAnonymous: DataTypes.BOOLEAN,
      isPublic: DataTypes.BOOLEAN,
      isFinished: DataTypes.BOOLEAN,
      isPublished: DataTypes.BOOLEAN,
      likeCount: {
        type: DataTypes.INTEGER,
        defaultValue: 0
      },
      finishedAt: DataTypes.DATE
    },
    {
      associate: function(models){
        Story.belongsTo(models.User);
        Story.hasMany(models.Post);
        Story.hasMany(models.Like);
      }
    }
  );

  return Story;
};

Authorific REST API
======
##Users
`GET /users/:facebookUserId`
**Gets** details of a user based on their facebook account

**Returns** a JSON object of the user

Sampe response schema:
```javascript
{
  "id": STRING,
  "name": STRING,
  "picture": {
    "data": {
      "is_silhouette": BOOLEAN,
      "url": STRING
    }
  }
}
```

##Stories and Posts
`GET /stories`

**Gets** stories based on the specifications of the request.

**Returns** a JSON Array of stories and their corresponding user information.

Sample request params:
```
/stories/?type=<STRING>&offset=<INTEGER>&limit=<INTEGER>
types: "popular", "trending", "new" or "ongoing"
```

Sample response schema:
```javascript
[
  {
    "id":INT,
    "title":STRING,
    "maxPost":INT,
    "imageUrl":STRING,
    "isAnonymous":BOOLEAN,
    "isPublic":BOOLEAN,
    "isFinished":BOOLEAN,
    "isPublished":BOOLEAN,
    "finishedAt":DATETIME,
    "createdAt":DATETIME,
    "updatedAt":DATETIME,
    "facebookObjectId":STRING,
    "facebookPostId":STRING,
    "UserId":INT,
    "likeCount":INT,
    "isLiked":BOOLEAN,
    "User":{
      "id":INT,
      "facebookUserId":STRING,
      "createdAt":DATETIME,
      "updatedAt":DATETIME
    },
    "Posts":[
      {
        "id":INT,
        "content":STRING,
        "postNumber":INT,
        "isAnonymous":BOOLEAN,
        "createdAt":DATETIME,
        "updatedAt":DATETIME,
        "UserId":INT,
        "StoryId":INT,
        "User":{
          "id":INT,
          "facebookUserId":STRING,
          "createdAt":DATETIME,
          "updatedAt":DATETIME
        }
      },
      {
        ...
      }
    ]
  },
  {
    ...
  }
]
```

`POST /stories`

**Creates** a new story.

**Requires** user to be logged in.

**Returns** a JSON Object of the story object if successful, else returns code `500`.

Sample `POST` schema:
```javascript
{
  "title":STRING,
  "maxPost":INT,
  "isAnonymous":BOOLEAN,
  "firstPost":STRING,
  "imageUrl":STRING (OPTIONAL)
}
```

Sample response schema:
```javascript
{
  "id":INT,
  "title":STRING,
  "maxPost":INT,
  "imageUrl":STRING,
  "isAnonymous":BOOLEAN,
  "isPublic":BOOLEAN,
  "isFinished":BOOLEAN,
  "isPublished":BOOLEAN,
  "finishedAt":DATETIME,
  "createdAt":DATETIME,
  "updatedAt":DATETIME,
  "UserId":INT
}
```

`GET /stories/:storyId`

**Gets** a specific story by its id.

**Returns** a JSON Object containing the story object and its corresponding user information, and an array of all posts related to that story and their corresponding user information.

Sample response schema:
```javascript
{
  "id":INT,
  "title":STRING,
  "maxPost":INT,
  "imageUrl":STRING,
  "isAnonymous":BOOLEAN,
  "isPublic":BOOLEAN,
  "isFinished":BOOLEAN,
  "isPublished":BOOLEAN,
  "finishedAt":DATETIME,
  "createdAt":DATETIME,
  "updatedAt":DATETIME,
  "facebookObjectId":STRING,
  "facebookPostId":STRING,
  "UserId":INT,
  "likeCount":INT,
  "isLiked":BOOLEAN,
  "User":{
    "id":INT,
    "facebookUserId":STRING,
    "createdAt":DATETIME,
    "updatedAt":DATETIME
  },
  "Posts":[
    {
      "id":INT,
      "content":STRING,
      "postNumber":INT,
      "isAnonymous":BOOLEAN,
      "createdAt":DATETIME,
      "updatedAt":DATETIME,
      "UserId":INT,
      "StoryId":INT,
      "User":{
        "id":INT,
        "facebookUserId":STRING,
        "createdAt":DATETIME,
        "updatedAt":DATETIME
      }
    },
    {
      ...
    }
  ]
}
```

`POST /stories/:storyId`

**Creates** a new post for the story with corresponding storyId. If it is the last possible post for the story, the story's isFinished value will be set to `true`.

**Requires** user to be logged in.

**Requires** the story to still be unfinished, returns error code `418` with status message 'Story has been marked as finished'.

**Requires** the postNumber to be the next in the series of postNumbers, i.e. if the current highest postNumber is 8, the object sent in the `POST` should be 9.

**Requires** the owner of the last post in the list not be owned by the same user.

**Returns** a JSON Object of the post object if successful.

**Returns** error code `409 Conflict` if there is a postNumber error.

Sample `POST` schema:
```javascript
{
  "content":STRING,
  "postNumber":INT,
  "isAnonymous":BOOLEAN
}
```
Sample response schema:
```javascript
{
  "id":INT,
  "content":STRING,
  "postNumber":INT,
  "isAnonymous":BOOLEAN,
  "createdAt":DATETIME,
  "updatedAt":DATETIME,
  "UserId":INT,
  "StoryId":INT
}
```

`PUT /stories/:storyId`

**Updates** the story's isFinished value

**Requires** user to be logged in.

**Requires** user to be owner of story.

**Returns** a JSON Object of the updated story if successful, else returns code `500`.

Sample response schema:
```javascript
{
  "id":INT,
  "title":STRING,
  "maxPost":INT,
  "imageUrl":STRING,
  "isAnonymous":BOOLEAN,
  "isPublic":BOOLEAN,
  "isFinished":BOOLEAN,
  "isPublished":BOOLEAN,
  "facebookObjectId":STRING,
  "facebookPostId":STRING,
  "finishedAt":DATETIME,
  "createdAt":DATETIME;
  "updatedAt":DATETIME,
  "UserId":INT
}
```

`DELETE /stories/:storyId`

**Deletes** the story with corresponding storyId

**Requires** user to be logged in.

**Requires** user to be owner of story.

**Requires** the story to still be unfinished, returns error code `418` with status message 'Story has been marked as finished'.

**Returns** a JSON Object of the deleted story if successful, else returns code `500`.

Sample response schema:
```javascript
{
  "id":INT,
  "title":STRING,
  "maxPost":INT,
  "imageUrl":STRING,
  "isAnonymous":BOOLEAN,
  "isPublic":BOOLEAN,
  "isFinished":BOOLEAN,
  "isPublished":BOOLEAN,
  "facebookObjectId":STRING,
  "facebookPostId":STRING,
  "finishedAt":DATETIME,
  "createdAt":DATETIME;
  "updatedAt":DATETIME,
  "UserId":INT
}
```

`PUT /stories/:storyId/like`

**Likes/Unlikes** the story corresponding to storyId.

**Requires** user to be logged in.

**Returns** a JSON Object of the updated like data, else returns code `500`.

Sample response schema:
```javascript
{
  "likeCount":INT,
  "isLiked":BOOLEAN
}
```

`GET /stories/:storyId/comment`

**GETS** the comments of a story corresponding to storyId.

**Returns** a JSON Object array of all the comments ordered by chronological order, else returns code `500`.

Sample response schema:
```javascript
[
  {
    "from":{
      "name":STRING,
      "id":STRING
    },
    "message":STRING,
    "created_time"DATETIME,
    "id":STRING
  }
]
```

`PUT /stories/:storyId/posts/:postNumber`

**Updates** a specific post by its postNumber and storyId.

**Requires** user to be logged in.

**Requires** user to be owner of story.

**Requires** the story to still be unfinished, returns error code `418` with status message 'Story has been marked as finished'.

**Requires** the post to be the lastest in the series of posts.

**Returns** a JSON Object of the updated post if successful, else returns code `500`.

Sample `PUT` schema:
```javascript
{
  "content":STRING
}
```
Sample response schema:
```javascript
{
  "id":INT,
  "content":STRING,
  "postNumber":INT,
  "isAnonymous":BOOLEAN,
  "createdAt":DATETIME,
  "updatedAt":DATETIME,
  "UserId":INT,
  "StoryId":INT
}
```

`DELETE /stories/:storyId/posts/:postNumber`

**Deletes** the post with corresponding storyId and postNumber.

**Requires** user to be logged in.

**Requires** user to be owner of post.

**Requires** the post not be the original post that was created with this story.

**Requires** the post to be the lastest in the series of posts.

**Returns** a JSON Object of the deleted post if successful, else returns code `500`.

Sample response schema:
```javascript
{
  "id":INT,
  "content":STRING,
  "postNumber":INT,
  "isAnonymous":BOOLEAN,
  "createdAt":DATETIME,
  "updatedAt":DATETIME,
  "UserId":INT,
  "StoryId":INT
}
```
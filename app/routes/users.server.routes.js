
'use strict';

/**
* Module dependencies.
*/
var passport = require('passport');

module.exports = function(app) {
  // User Routes
  var users = require('../../app/controllers/users');

  // User Routes
  app.get('/signin', users.signin);
  app.get('/signup', users.signup);
  app.post('/signout', users.signout);
  app.get('/users/me', users.me);
  app.get('/users/:facebookUserId', users.getFacebookData);

  // Setting up the users api
  app.post('/users', users.create);

  // Setting the facebook oauth routes
  app.get('/auth/facebook', passport.authenticate('facebook', {
    scope: ['publish_actions'],
    display: 'popup',
    failureRedirect: '/signin'
  }), users.signin);

  app.get('/auth/facebook/callback', passport.authenticate('facebook', {
    scope: ['publish_actions'],
    successRedirect: '/views/state/success.html',
    failureRedirect: '/signin'
  }), users.authCallback);

  // Finish with setting up the userId param
  app.param('facebookUserId', users.user);
};


'use strict';

/**
* Module dependencies.
*/
var users = require('../../app/controllers/users'),
stories = require('../../app/controllers/stories');

module.exports = function(app) {
// Story Routes
app.route('/stories')
    .get(stories.removeNotModified, stories.getStories, stories.filterData)
    .post(users.requiresLogin, stories.validateStory, stories.createStory);
app.route('/stories/:storyId')
    .get(stories.removeNotModified, stories.showStory)
    .post(users.requiresLogin, stories.isStoryUnfinished, stories.isPrevPostNotOwn, stories.isNewPostNumberValid, stories.validatePost, stories.createPost)
    .put(users.requiresLogin, stories.hasStoryAuthorization, stories.markStoryFinished)
    .delete(users.requiresLogin, stories.hasStoryAuthorization, stories.destroyStory);
app.route('/stories/:storyId/posts/:postNumber')
    .put(users.requiresLogin, stories.hasPostAuthorization, stories.isStoryUnfinished, stories.isLastPost, stories.updatePost)
    .delete(users.requiresLogin, stories.hasPostAuthorization, stories.isStoryUnfinished, stories.isNotOriginalPost, stories.isLastPost, stories.destroyPost);
app.route('/stories/:storyId/like')
	.put(users.requiresLogin, stories.likeStory);
app.route('/stories/:storyId/share')
    .post(users.requiresLogin, stories.share);
app.route('/stories/:storyId/comments')
    .get(stories.removeNotModified, stories.getFacebookComments);

// Finish with setting up the storyId or postId param
// Note: the stories.story function will be called everytime then it will call the next function.
app.param('storyId', stories.story);
app.param('postNumber', stories.post);
};
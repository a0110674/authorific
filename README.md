Authorific
======
##Basic information
###Authorific's repository using M*EAN Stack
##Setting developer environment
 - Clone the repository locally
 - Run '**npm install**' and '**bower install**'
 - Enter MySQL configurations into **/config/env/development.js**
 - Open console and run '**grunt**' from root
 - Access the application from 'localhost:3000'
##Notes
- head.jade, default.jade and foot.jade are served by ExpressJS upon load
- All CSS related files are to be included into **head.jade** while all JS related files are to be included into **foot.jade**
- **default.jade** serves **header.html**, **ui-view** (main body for page depending on current state from ui-router) and **footer.html**
- All AngularJS files can be found in the **public** folder
'use strict';

/**
  * @ngdoc function
  * @name authorific.directive: picture-modal 
  * @description
  * # picture-modal
  * Directive to display an image
  */

angular.module('authorific').directive('authorificPictureModal', function($mdDialog) {

  return {
    restrict: 'E',
    scope: {
      image: '@'
    },
    templateUrl: '/views/template/picture-thumbnail.html',
    link: function($scope) {

      $scope.openPictureModal = function(ev, image) {

        function PictureModalController($scope, $mdDialog, $rootScope) {
          $scope.image = image;
          $scope.closeDialog = function() {
            $mdDialog.hide();
          }
        }

        $mdDialog.show({
          templateUrl: '/views/template/picture-modal.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          controller: PictureModalController
        });
      };
    }

  };
});

'use strict';

/**
  * @ngdoc function
  * @name authorific.directive: always-on-sidebar
  * @description
  * # always-on-sidebar
  * Directive to keep sidebar in viewport at all times
  */

angular.module('authorific').directive('authorificAlwaysOnSidebar', function($window) {

  return {
    restrict: 'C',
    link: function(scope, element) {
		var viewportY;
		var elementY = element[0].offsetTop;

		//Setting to position fixed will alter the width, so we need the original width
		var originalWidth = element[0].offsetWidth;

		//When resize, need to recompute the variables again
		angular.element($window).bind('resize', function() {
			elementY = element[0].offsetTop;
			originalWidth = element[0].offsetWidth;
		});


		angular.element($window).bind('scroll', function() {
			viewportY = $window.scrollY;
			if(elementY < viewportY) {
				element.css('position', 'fixed');
				element.css('top', '0px');
				element.css('width', originalWidth + 'px');
			}
			else {
				element.css('position', 'static');
				element.css('top', 'initial');
				element.css('width', 'initial');
			}
        });
    }
  };
});


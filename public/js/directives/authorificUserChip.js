'use strict';

/**
  * @ngdoc function
  * @name authorific.directive: user-chip 
  * @description
  * # user-chip
  * Directive to display a user in a badge form given his information
  */

angular.module('authorific').directive('authorificUserChip', function() {

  return {
    restrict: 'E',
    scope: {
      username: '@',
      profilePicture: '@',
      colorClass: '@'
    },
    templateUrl: '/views/template/authorific-user-chip.html'
  };
});

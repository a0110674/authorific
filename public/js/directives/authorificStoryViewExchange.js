'use strict';

/**
  * @ngdoc function
  * @name authorific.directive: story-view-exchange
  * @description
  * # story-view-exchange
  * Directive to change story view
  */

angular.module('authorific').directive('authorificStoryViewExchange', function($timeout) {

  return {
    restrict: 'A',
    link: function(scope, element) {
      //Default view of story
      var state = 0;

      scope.$on('click', function() {
        console.log('hi');
      });
      
      element.on('click', function() {
        console.log('bye');
      });

      console.log('loaded');

    }
  };
});
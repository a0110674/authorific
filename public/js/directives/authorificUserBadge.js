'use strict';

/**
  * @ngdoc function
  * @name authorific.directive: user-badge 
  * @description
  * # user-badge
  * Directive to display a user in a badge form given his information
  */

angular.module('authorific').directive('authorificUserBadge', function() {

  return {
    restrict: 'E',
    scope: {
      username: '@',
      profilePicture: '@',
      status: '@'
    },
    templateUrl: '/views/template/authorific-user-badge.html'
  };
});

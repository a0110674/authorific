'use strict';

/**
 * @ngdoc function
 * @name authorific.controller:StoryViewCtrl
 * @description
 * # StoryViewCtrl
 * Controller of the authorific story view
 */
angular.module('authorific')
.controller('StoryViewController', function ($scope, $state, $stateParams, $element, $window, $mdDialog, $mdToast, authorificBackend) {

  //Determine the id of the story to load
  var storyId = $stateParams.storyId;

  //Load the story in
  authorificBackend.getStory($stateParams.storyId).$promise.then(function(response) {
    //Show the story
    $scope.storyExist = true;
    //Load the story into $scope.story after processing
    $scope.story = response;
    console.log($scope.story);
  });

});
'use strict';

/**
 * @ngdoc function
 * @name authorific.controller:CreateCtrl
 * @description
 * # CreateCtrl
 * Controller of the authorific
 * Todo: 
 * Reinforce the word count
 * Redirect after successful creation
 */
angular.module('authorific')
  .controller('CreateController', function ($scope, $rootScope, $state, $http, $location, $mdToast, $mdDialog, imgur) {
  	//Mock data

    if (!$scope.userAuthenticated) {
      $location.path('/');
    }
  	
  	$scope.title = "";
    $scope.story = "";
    $scope.privacy = "Public";
  	$scope.isPublic = true;
    $scope.isAnonymous = false;
    $scope.postLimit = 10;

    $scope.genres        = loadAll();
    $scope.querySearch   = querySearch;
    $scope.selectedGenres = [];
    $scope.selectedGenre = "";

    function querySearch (query) {
      var results = query ? $scope.genres.filter( createFilterFor(query) ) : $scope.genres, deferred;
      return results;      
    }
    
    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
      var allGenres = 'Action, Adventure, Comedy, Crime, Fantasy, History, Horror, Philosophy, Romance, Sci-fi, Twist';
      return allGenres.split(/, +/g).map( function (genre) {
        return {
          value: genre.toLowerCase(),
          display: genre
        };
      });
    }
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(state) {
        return (state.value.indexOf(lowercaseQuery) === 0);
      };
    }

  	$scope.switchPrivacy = function() {
  		if($scope.isPublic) {
  			$scope.isPublic = false;
        $location.path('/');
  		}
  		else {
  			$scope.isPublic = true;	
  		}
  	}

  	$scope.submit = function(title, story) {
      //Validation

      console.log(title);
      console.log(story);

      if(title === undefined || title === null || title === '' || title.length > 180) {
        console.log('reject')
        $mdToast.show({
          template: '<md-toast class="md-toast-error"><span><i class="fa fa-exclamation-triangle"></i> Please make sure your title is within 180 characters</span></md-toast>',
          hideDelay: 2000,
          position: 'bottom left'
        });
        return;
      }

      if(story === undefined || story === null || story === '' || story.length > 180) {
        console.log('reject')
        $mdToast.show({
          template: '<md-toast class="md-toast-error"><span><i class="fa fa-exclamation-triangle"></i> Please make sure your post is within 180 characters</span></md-toast>',
          hideDelay: 2000,
          position: 'bottom left'
        });
        return;
      }

      $scope.disableSubmit = true;

      if($scope.croppedImage !== undefined && $scope.croppedImage !== null) {
        uploadImgur($scope.croppedImage, postStory)
      }

      else {
        postStory();
      }

  	}

    var postStory = function(url) {
      if(url === undefined) {
        $http.post('/stories', {title: $scope.title, maxPost: $scope.postLimit, isAnonymous: $scope.isAnonymous, firstPost: $scope.story}).
        then(function(response) {
          $mdToast.show($mdToast.simple().content('Your story has been successfuly submitted!'));
          $location.path('/ongoing');
        }, function(response) {
          $mdToast.show($mdToast.simple().content('Oops! There seems to be a problem.'));
          $scope.disableSubmit = true;
        });
      }
      else {
        $http.post('/stories', {title: $scope.title, maxPost: $scope.postLimit, isAnonymous: $scope.isAnonymous, firstPost: $scope.story, imageUrl: url}).
        then(function(response) {
          $mdToast.show($mdToast.simple().content('Your story has been successfuly submitted!'));
          $location.path('/ongoing');
        }, function(response) {
          $mdToast.show($mdToast.simple().content('Oops! There seems to be a problem.'));
          $scope.disableSubmit = true;
        });
      }

    }

    function uploadImgur(croppedImage, callback) {
      var b64Data = croppedImage.split(',')[1];
      var blob = b64toBlob(b64Data);
      imgur.upload(blob).then(function(model) {
        callback(model.link)
      });
    }

    function b64toBlob(b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);

          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
          }

          var byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    }

    $scope.$watch(function() {
      return $scope.cropper === undefined ? null : $scope.cropper.sourceImage;
    }, function(newImage) {
      if(newImage !== null && newImage.substring(0,10) === 'data:image') {
        showCropper(newImage);
      }
    });

    $scope.$on('cropped-image-ready', function(evt, args) {
      $scope.croppedImage = args.croppedImage;
    });

    function showCropper(sourceImage) {
      $mdDialog.show({
        controller: 'CropperController',
        templateUrl: '/views/template/cropper.html',
        parent: angular.element(document.body),
      });

      $scope.$on('cropper-controller-ready', function() {
        $rootScope.$broadcast('crop-picture', {'sourceImage': sourceImage});
      });
    }

    $scope.showFilePicker = function() {
      angular.element("#file-input").trigger("click");
    }
  });
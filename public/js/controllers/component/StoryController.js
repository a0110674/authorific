'use strict';

/**
 * @ngdoc function
 * @name authorific.controller:StoryCtrl
 * @description
 * # StoryCtrl
 * Controller of the authorific stories
 */
angular.module('authorific')
.controller('StoryController', function ($scope, $rootScope, $state, $stateParams, $element, $window, Facebook, $mdDialog, $mdToast, authorificBackend) {

  //Load facebook information of all posts' authors and creator
  //Compute the date
  //Get unique set of co-authors
  var processStory = function(story) {
    // Load comments
    story.commentsVisible = $state.current.name === "story";
    story.loadingComments = false;
    getStoryComments(story);

    //Load facebook information of creator
    if(story.User !== null) {
      getFacebookUser(story.User.facebookUserId, function(response) {
        story.User.facebookInformation = response;
      });
    }

    story.coAuthors = [];
    //Load facebook information of posts' authors and push into coAuthors array
    for(var i = 0; i < story.Posts.length; i++) {
      if(story.Posts[i].User !== null) {
        getFacebookUser(story.Posts[i].User.facebookUserId, function(response, toAppend) {
          toAppend.facebookInformation = response;
        }, story.Posts[i].User);  
      }
      story.coAuthors.push(story.Posts[i].User);
    }

    //Assign one of the color classes to each coAuthors

    //Compute the date
    story.publishedDate = new Date(story.finishedAt).getTime();

    //Get unique coauthors
    story.coAuthors = _.unique(story.coAuthors, 'facebookUserId');

    for(var i = 0; i < story.coAuthors.length; i++) {
      if(story.coAuthors[i] !== null) {
        story.coAuthors[i].colorClass = randomColorGenerator();
        story.coAuthors[i].colorClassHover = story.coAuthors[i].colorClass + 'Hover'; 
      }
      else {
        //Generate a random color for anonymous users
        story.anonymous = {};
        story.anonymous.colorClass = randomColorGenerator();
        story.anonymous.colorClassHover = story.anonymous.colorClass + 'Hover';
        story.coAuthors[i] = story.anonymous;
      }
    }

    for(var i = 0; i < story.Posts.length; i++) {
      if(story.Posts[i].User !== null && story.Posts[i].User !== undefined) {
        for(var j = 0; j < story.coAuthors.length; j++) {
          if(story.coAuthors[j] !== null && story.coAuthors[j] !== undefined) {  
            if(story.Posts[i].User.facebookUserId === story.coAuthors[j].facebookUserId) {
              story.Posts[i].User = story.coAuthors[j];
            }
          }
        }
      } else {
        story.Posts[i].User = story.anonymous;
      }
    }
  };


  //Get a facebook user information and perform the callback, if the target is specified, it is passed into the callback as well
  var getFacebookUser = function(facebookUserId, callback, target) {
  authorificBackend.getFacebookUser(facebookUserId).$promise.then(
    function(response) {
      if(target !== undefined) {
        callback(response, target);
      }
      else {
        callback(response);
      }
    }
  )};

  var randomColorGenerator = function() {
    //Generate a random nice color
    var max = 210;
    var min = 75;
    var red = Math.floor(((Math.random() * (max-min) + min) + 255) / 2);
    var green = Math.floor(((Math.random() * (max-min) + min) + 255) / 2);
    var blue = Math.floor(((Math.random() * (max-min) + min) + 255) / 2);

    //Plop it into CSS
    colorStyle[0].innerHTML += '.colorClass' + colorCount + ' {background-color : rgb('+red+','+green+','+blue+')}';
    colorStyle[0].innerHTML += '.colorClass' + colorCount + 'Hover:hover {background-color : rgb('+red+','+green+','+blue+')}';


    //CSS style
    var style = 'colorClass' + colorCount;

    colorCount++;

    //Return the CSS style for that color
    return style;
  }

  //Function to load multiple stories
  var loadStories = function(callback) {
    authorificBackend.getStories(type, offset, numberOfStoriesToLoad).$promise.then(function(response) {
      //Update infinite scroll offset and pause infinite-scroll
      offset += response.length;
      $scope.stopInfiniteScroll = false;
      //Initialize and load the stories into $scope.stories after processing
      for(var i = 0; i < response.length; i++) {
        processStory(response[i]);
        $scope.stories.push(response[i]);
      }
      if(callback !== undefined) {
        callback();
      }
    });
  }

  //Function for infinite-scroll to call
  $scope.loadMoreStories = function() {
    $scope.stopInfiniteScroll = true;
    var initialOffset = offset;
    loadStories(function() {
      if(offset === initialOffset) {
        $scope.stopInfiniteScroll = true;
      }
      else {
        $scope.stopInfiniteScroll = false;
      }
    });
  }

  $scope.likeStory = function(story) {

    if(!isAuthenticated()) {
      return;
    }

    authorificBackend.likeStory(story.id).$promise.then(function(response){
      story.likeCount = response.likeCount;
      story.isLiked = response.isLiked;
    });
  }

  $scope.openShareDialog = function($event, story) {

    if(!isAuthenticated()) {
      return;
    }

    Facebook.ui({
      method: 'share_open_graph',
      display: 'popup',
      action_type: 'authorific:recommend',
      action_properties: JSON.stringify({
        story: story.facebookObjectId
      })
    }, function(response) {
      
    });
  }

  $scope.openFacebookPost = function(facebookPostId) {
    $window.open('https://www.facebook.com/' + facebookPostId, '_blank');
  };

  //Number of colors and add stylesheet for colors
  var colorCount = 0;
  var colorStyle = $('style');
  colorStyle.type = 'text/css';
  $('head').append(colorStyle);
  console.log($('head'));
  //Load type of state into scope
  $scope.state = $state.current.name;

  //Determine which state(home/story)
  if($state.current.name === 'home') {
    //Determine which type(popular/trending/new)
    var type = $stateParams.type;

    //Set a container for stories
    $scope.stories = [];

    //Switch on infinite scrolling
    var offset = 0;
    var numberOfStoriesToLoad = 10;
    $scope.stopInfiniteScroll = true;

    loadStories();
  }
  else if($state.current.name === 'story'){
    //Determine the id of the story to load
    var storyId = $stateParams.storyId;

    //Load the story in
    authorificBackend.getStory($stateParams.storyId).$promise.then(function(response) {
      //Show the story
      $scope.storyExist = true;
      //Load the story into $scope.story after processing
      processStory(response);
      $scope.story = response;
    });
  }

  function getStoryComments(story) {
    story.comments = [];
    story.loadingComments = true;
    authorificBackend.getStoryComments(story.id).$promise.then(function(response){
      story.loadingComments = false;
      for (var i = 0; i < response.data.length; i++) {
        processComment(response.data[i]);
        story.comments.push(response.data[i]);
      }
      story.commentCount = response.summary.total_count;
    }, function(error) {
      story.loadingComments = false;
      story.commentCount = 0;
    });
  }

  function processComment(comment) {
    comment.date = new Date(comment.created_time).getTime();
    getFacebookUser(comment.from.id, function(response, target){
      comment.profile_picture = response.picture.data.url;
    }, comment);
  }

  $scope.showComments = function(story) {

    if(story.commentsVisible) {
      story.commentsVisible = false;
    }
    else {
      story.commentsVisible = true;
    }

  }

  $scope.isToday = function(date) {
    var todayDate = new Date();
    if(date.setHours(0,0,0,0) === todayDate.setHours(0,0,0,0)){
      return true;
    }
    else {
      return false;
    }
  }

  $scope.openPictureModal = function(ev, image) {

    function PictureModalController($scope, $mdDialog, $rootScope) {
      $scope.image = image;
      $scope.closeDialog = function() {
        $mdDialog.hide();
      }
    }

    $mdDialog.show({
      templateUrl: '/views/template/picture-modal.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      controller: PictureModalController
    });
  };

  function isAuthenticated() {
    return $scope.user !== null;
  }

});
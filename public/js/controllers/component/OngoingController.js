'use strict';

/**
 * @ngdoc function
 * @name authorific.controller:OngoingController
 * @description
 * # Ongoing controller
 * Controller of the ongoing view
 * [
 *   {
 *     'id':INT,
 *     'title':STRING,
 *     'storyType':INT,
 *     'maxPost':INT,
 *     'isAnonymous':BOOLEAN,
 *     'isPublic':BOOLEAN,
 *     'isFinished':BOOLEAN,
 *     'isPublished':BOOLEAN,
 *     'createdAt':DATETIME;
 *     'updatedAt':DATETIME,
 *     'UserId':INT,
 *     'User':{
 *       'id':INT,
 *       'facebookUserId':INT,
 *       'createdAt':DATETIME,
 *       'updatedAt':DATETIME
 *     }
 *   },
 *   {
 *    ...
 *   }
 * ]
 * Todo:
 * Only show ongoing for posts from which recent posts are not made by user
 * Only show posts when user has not made a post since page load
 * Disable interaction if not login (Need to discuss facebook implementation)
 */
angular.module('authorific')
  .controller('OngoingController', function ($scope, $state, $location, $mdToast, $mdDialog, authorificBackend) {
    var offset = 0;
    var numberOfStoriesToLoad = 10;
    var user;

    if (!$scope.userAuthenticated) {
      $location.path('/');
    }

    $scope.stopInfiniteScroll = true;
    $scope.stories = [];
    $scope.tabSelected = 3;

    var loadOngoingStories = function(callback) {
      authorificBackend.getStories('ongoing', offset, numberOfStoriesToLoad).$promise.then(function(response) {
              
        //Get all the stories in
        for(var i = 0; i < response.length; i++) {
          $scope.stories.push(response[i]);
        }
        //Compute new offset
        offset += response.length;
        //Start infinitescroll
        $scope.stopInfiniteScroll = false;
        //Compute most recent post and facebook information and whether can delete/edit or post and if creator is anonymous or not
        var posts;
        for(var i = 0; i < $scope.stories.length; i++) {
          //Recent post
          posts = $scope.stories[i]['Posts'];
          $scope.stories[i].recentPost = posts[posts.length-1];
          
          //Get facebook information if user not null
          if($scope.stories[i].User !== null) {
            getSingleFacebookUser($scope.stories[i], $scope.stories[i].User.facebookUserId);
          }
          //Can edit or not
          if($scope.stories[i].recentPost.User && $scope.stories[i].recentPost.User.facebookUserId === user.facebookUserId) {
            $scope.stories[i].canPost = false;
          }
          else {
            $scope.stories[i].canPost = true; 
          }
        }
        if(callback !== undefined) {
          callback(response);
        }

      });
    }

    var getSingleFacebookUser = function(toAppend, facebookUserId) {
      authorificBackend.getFacebookUser(facebookUserId).$promise.then(function(response) {
        toAppend.facebookInformation = response;   
      });
    }

    $scope.createPost = function($index, event, storyId, content, mostRecentPostNumber, isAnonymous) {

      if(content === undefined || content === null || content === '' || content.length > 180) {
        console.log('reject')
        $mdToast.show({
          template: '<md-toast class="md-toast-error"><span><i class="fa fa-exclamation-triangle"></i> Please make sure your post is within 180 characters</span></md-toast>',
          hideDelay: 2000,
          position: 'bottom left'
        });
      }
      else {        
        authorificBackend.createPost(storyId, content, mostRecentPostNumber + 1, isAnonymous)
        .$promise.then(successHandler(storyId, $index), errorHandler(event, storyId));
      }
    }

    $scope.editPost = function($index, event, storyId, content, mostRecentPostNumber) {
      if(content === undefined || content === null || content === '' || content.length > 180) {
        console.log('reject')
        $mdToast.show({
          template: '<md-toast class="md-toast-error"><span><i class="fa fa-exclamation-triangle"></i> Please make sure your post is valid</span></md-toast>',
          hideDelay: 2000,
          position: 'bottom left'
        });
      }
      else {
        authorificBackend.updatePost(storyId, content, mostRecentPostNumber)
        .$promise.then(successHandler(storyId, $index), errorHandler(event, storyId));
      }
    }

    $scope.deletePost = function($index, event, storyId, mostRecentPostNumber) {
      authorificBackend.deletePost(storyId, mostRecentPostNumber)
      .$promise.then(successHandler(storyId, $index), errorHandler(event, storyId));
      $scope.goBackToChoice($index);
    }

    $scope.loadMoreOngoing = function() {
      $scope.stopInfiniteScroll = true;
      var initialOffset = offset;
      loadOngoingStories(function(response) {
        if(offset === initialOffset) {
          $scope.stopInfiniteScroll = true;
        }
        else {
          $scope.stopInfiniteScroll = false;
        }
      });
    }

    //Array of booleans for the showing and hiding of elements
    $scope.hideChoice = [];
    $scope.showEdit = [];
    $scope.showDelete = [];

    //Return to choice form for a particular story
    $scope.goBackToChoice = function($index) {
      $scope.hideChoice[$index] = false;
      $scope.showEdit[$index] = false;
      $scope.showDelete[$index] = false;
    }

    //Show the edit form for a particular story
    $scope.goToEdit = function($index) {
      $scope.hideChoice[$index] = true;
      $scope.showEdit[$index] = true;
      $scope.showDelete[$index] = false;
    }

    //Show the delete form for a particular story
    $scope.goToDelete = function($index) {
      if($scope.stories[$index].recentPost.postNumber > 1) {
        $scope.hideChoice[$index] = true;
        $scope.showEdit[$index] = false;
        $scope.showDelete[$index] = true;
      }
    }

    var reloadStories = function() {
      $scope.stopInfiniteScroll = true;
      $scope.stories = [];
      $scope.hideChoice = [];
      $scope.showEdit = [];
      $scope.showDelete = [];
      offset = 0;
      loadOngoingStories();
    }

    var successHandler = function(storyId, $index) {
      $scope.goBackToChoice($index);
      return function(response) {
        reloadStory(storyId);
      };
    }

    var errorHandler = function(event, storyId){
      return function(response) {
        if (response.status === 409) {
          showReloadDialog(event, angular.element(document.body), storyId);
        } else if (response.status === 418) {
          showRemoveDialog(event, angular.element(document.body), storyId);
        }
      };
    }

    var reloadStory = function(storyId) {
      authorificBackend.getStory(storyId).$promise.then(function(response){
        if (response.isFinished) {
          return removeStory(storyId);
        }
        for (var i = 0; i < $scope.stories.length; i++) {
          if ($scope.stories[i].id === storyId) {
            // edit story details
            $scope.stories[i]['Posts'] = response['Posts'];
            $scope.stories[i].recentPost = response['Posts'][response['Posts'].length-1];
            //Can edit or not
            if($scope.stories[i].recentPost.User && $scope.stories[i].recentPost.User.facebookUserId === user.facebookUserId) {
              $scope.stories[i].canPost = false;
            }
            else {
              $scope.stories[i].canPost = true; 
            }
            break;
          }
        }
      });
    }

    var removeStory = function(storyId) {
      for (var i = 0; i < $scope.stories.length; i++) {
        if ($scope.stories[i].id === storyId) {
          $scope.stories.splice(i, 1);
          break;
        }
      }
    }

    var showReloadDialog = function(targetEvent, parent, storyId) {
      $mdDialog.show({
        controller: ReloadDialogController,
        templateUrl: '/views/template/authorific-small-dialog.html',
        parent: parent,
        targetEvent: targetEvent,
        clickOutsideToClose: true
      }).then(function(){
        reloadStory(storyId);
      });
    }

    var showRemoveDialog = function(targetEvent, parent, storyId) {
      $mdDialog.show({
        controller: RemoveDialogController,
        templateUrl: '/views/template/authorific-small-dialog.html',
        parent: parent,
        targetEvent: targetEvent,
        clickOutsideToClose: true
      }).then(function(){
        removeStory(storyId);
      });
    }

    function ReloadDialogController ($scope, $mdDialog) {
      $scope.content = "It seems like this story is not up to date.";
      $scope.acceptText = "Reload";
      $scope.cancelText = "Close";

      $scope.accept = function() {
        $mdDialog.hide();
      }

      $scope.cancel = function() {
        $mdDialog.cancel();
      }
    }

    function RemoveDialogController ($scope, $mdDialog) {
      $scope.content = "Someone has already finished this story.";
      $scope.acceptText = "Remove";
      $scope.cancelText = "Close";

      $scope.accept = function() {
        $mdDialog.hide();
      }

      $scope.cancel = function() {
        $mdDialog.cancel();
      }
    }

    //Open up the choice form
    $scope.finishStoryConfirmation = function(story, post, isAnonymous) {
      story.confirmFinish = true;
      if(post === undefined || post === '') {
        story.noLastPost = true;
      }
      else {
        story.lastPost = {};
        story.lastPost.content = post;
        story.lastPost.isAnonymous = isAnonymous;
      }
    }

    //Return to post form for a particular story
    $scope.goBackToPost = function(story) {
      story.confirmFinish = false;
      story.noLastPost = false;
    }

    $scope.finishStory = function(story, $index) {
      //If there is a last post, post it first and then finish the story
      if(!story.noLastPost) {
        authorificBackend.createPost(story.id, story.lastPost.content, story.Posts.length + 1, story.lastPost.isAnonymous).$promise.then(function() {
          authorificBackend.finishStory(story.id).$promise.then(function() {
            $scope.stories.splice($index, 1);
          });
        });
      }
      else {
        authorificBackend.finishStory(story.id).$promise.then(function() {
            $scope.stories.splice($index, 1);
        });
      }
    }

    user = authorificBackend.getUser();
    loadOngoingStories();
 }
);
'use strict';

/**
 * @ngdoc function
 * @name authorific.controller:StoryViewOngoingController
 * @description
 * # Ongoing controller
 * Controller of the ongoing view
 */
angular.module('authorific')
  .controller('StoryViewOngoingController', function ($scope, $state, $location, $mdToast, $mdDialog, authorificBackend) {

    if (!$scope.userAuthenticated) {
      $location.path('/');
    }

    var processStory = function(story) {
      //Recent post
      var posts = story['Posts'];
      story.recentPost = posts[posts.length-1];
      
      //Get facebook information if user not null
      if(story.User !== null) {
        getSingleFacebookUser(story, story.User.facebookUserId);
      }
      //Can edit or not
      if(story.recentPost.User && story.recentPost.User.facebookUserId === user.facebookUserId) {
        story.canPost = false;
      }
      else {
        story.canPost = true; 
      }
    }

    var getSingleFacebookUser = function(toAppend, facebookUserId) {
      authorificBackend.getFacebookUser(facebookUserId).$promise.then(function(response) {
        toAppend.facebookInformation = response;   
      });
    }

    $scope.createPost = function($index, event, storyId, content, mostRecentPostNumber, isAnonymous) {
      if(content === undefined || content === '') {
        $mdToast.show({
          template: '<md-toast class="md-toast-error"><span><i class="fa fa-exclamation-triangle"></i> Please make sure your post is valid</span></md-toast>',
          hideDelay: 2000,
          position: 'bottom left'
        });
      }
      else {
        authorificBackend.createPost(storyId, content, mostRecentPostNumber + 1, isAnonymous)
        .$promise.then(successHandler(storyId), errorHandler(event, storyId));
      }
      $scope.goBackToChoice($index);
    }

    $scope.editPost = function($index, event, storyId, content, mostRecentPostNumber) {
      if(content === undefined || content === '') {
        $mdToast.show({
          template: '<md-toast class="md-toast-error"><span><i class="fa fa-exclamation-triangle"></i> Please make sure your post is valid</span></md-toast>',
          hideDelay: 2000,
          position: 'bottom left'
        });
      }
      else {
        authorificBackend.updatePost(storyId, content, mostRecentPostNumber)
        .$promise.then(successHandler(storyId), errorHandler(event, storyId));
      }
      $scope.goBackToChoice($index);
    }

    $scope.deletePost = function($index, event, storyId, mostRecentPostNumber) {
      authorificBackend.deletePost(storyId, mostRecentPostNumber)
      .$promise.then(successHandler(storyId), errorHandler(event, storyId));
      $scope.goBackToChoice($index);
    }

    $scope.loadMoreOngoing = function() {
      $scope.stopInfiniteScroll = true;
      var initialOffset = offset;
      loadOngoingStories(function(response) {
        if(offset === initialOffset) {
          $scope.stopInfiniteScroll = true;
        }
        else {
          $scope.stopInfiniteScroll = false;
        }
      });
    }

    //Array of booleans for the showing and hiding of elements
    $scope.hideChoice = [];
    $scope.showEdit = [];
    $scope.showDelete = [];

    //Return to choice form for a particular story
    $scope.goBackToChoice = function($index) {
      $scope.hideChoice[$index] = false;
      $scope.showEdit[$index] = false;
      $scope.showDelete[$index] = false;
    }

    //Show the edit form for a particular story
    $scope.goToEdit = function($index) {
      $scope.hideChoice[$index] = true;
      $scope.showEdit[$index] = true;
      $scope.showDelete[$index] = false;
    }

    //Show the delete form for a particular story
    $scope.goToDelete = function($index) {
      if($scope.story.recentPost.postNumber > 1) {
        $scope.hideChoice[$index] = true;
        $scope.showEdit[$index] = false;
        $scope.showDelete[$index] = true;
      }
    }

    var reloadStories = function() {
      $scope.stopInfiniteScroll = true;
      $scope.stories = [];
      $scope.hideChoice = [];
      $scope.showEdit = [];
      $scope.showDelete = [];
      offset = 0;
      loadOngoingStories();
    }

    var successHandler = function(storyId) {
      return function(response) {
        reloadStory(storyId);
      };
    }

    var errorHandler = function(event, storyId){
      return function(response) {
        if (response.status === 409) {
          showReloadDialog(event, angular.element(document.body), storyId);
        } else if (response.status === 418) {
          showRemoveDialog(event, angular.element(document.body), storyId);
        }
      };
    }

    var reloadStory = function(storyId) {
      authorificBackend.getStory(storyId).$promise.then(function(response){
        if ($scope.story.id === storyId) {
          // edit story details
          $scope.story['Posts'] = response['Posts'];
          $scope.story.recentPost = response['Posts'][response['Posts'].length-1];
          //Can edit or not
          if($scope.story.recentPost.User && $scope.story.recentPost.User.facebookUserId === user.facebookUserId) {
            $scope.story.canPost = false;
          }
          else {
            $scope.story.canPost = true; 
          }
        }
      });
    }

    var refreshStory = function(storyId) {
      authorificBackend.getStory(storyId).$promise.then(function(response){
        $scope.story = response;
      });
    }

    var showReloadDialog = function(targetEvent, parent, storyId) {
      $mdDialog.show({
        controller: ReloadDialogController,
        templateUrl: '/views/template/authorific-small-dialog.html',
        parent: parent,
        targetEvent: targetEvent,
        clickOutsideToClose: true
      }).then(function(){
        reloadStory(storyId);
      });
    }

    var showRemoveDialog = function(targetEvent, parent, storyId) {
      $mdDialog.show({
        controller: RemoveDialogController,
        templateUrl: '/views/template/authorific-small-dialog.html',
        parent: parent,
        targetEvent: targetEvent,
        clickOutsideToClose: true
      }).then(function(){
        removeStory(storyId);
      });
    }

    function ReloadDialogController ($scope, $mdDialog) {
      $scope.content = "It seems like this story is not up to date.";
      $scope.acceptText = "Reload";
      $scope.cancelText = "Close";

      $scope.accept = function() {
        $mdDialog.hide();
      }

      $scope.cancel = function() {
        $mdDialog.cancel();
      }
    }

    function RemoveDialogController ($scope, $mdDialog) {
      $scope.content = "Someone has already finished this story.";
      $scope.acceptText = "Remove";
      $scope.cancelText = "Close";

      $scope.accept = function() {
        $mdDialog.hide();
      }

      $scope.cancel = function() {
        $mdDialog.cancel();
      }
    }

    //Open up the choice form
    $scope.finishStoryConfirmation = function(story, post, isAnonymous) {
      story.confirmFinish = true;
      if(post === undefined || post === '') {
        story.noLastPost = true;
      }
      else {
        story.lastPost = {};
        story.lastPost.content = post;
        story.lastPost.isAnonymous = isAnonymous;
      }
    }

    //Return to post form for a particular story
    $scope.goBackToPost = function(story) {
      story.confirmFinish = false;
      story.noLastPost = false;
    }

    $scope.finishStory = function(story, $index) {
      //If there is a last post, post it first and then finish the story
      if(!story.noLastPost) {
        authorificBackend.createPost(story.id, story.lastPost.content, story.Posts.length + 1, story.lastPost.isAnonymous).$promise.then(function() {
          authorificBackend.finishStory(story.id).$promise.then(function() {
            refreshStory($scope.story.id);
          });
        });
      }
      else {
        authorificBackend.finishStory(story.id).$promise.then(function() {
          refreshStory($scope.story.id);
        });
      }
    }

    user = authorificBackend.getUser();
    processStory($scope.story);
 }


);
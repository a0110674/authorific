'use strict';

/**
 * @ngdoc function
 * @name authorific.controller:CropperCtrl
 * @description
 * # CropperCtrl
 * Controller of the cropper
 * Todo: 
 * Reinforce the word count
 * Redirect after successful creation
 */
angular.module('authorific')
  .controller('CropperController', function ($scope, $rootScope, $mdDialog) {
    $scope.$on('crop-picture', function(evt, args) {
      $scope.sourceImage = args.sourceImage;
    });

    $rootScope.$broadcast('cropper-controller-ready');
    
    $scope.closeDialog = function() {
      $mdDialog.hide();
    }

    $scope.returnImage = function(croppedImage) {
      $rootScope.$broadcast('cropped-image-ready', {'croppedImage' : croppedImage});
      $mdDialog.hide();
    }

  });
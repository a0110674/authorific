angular.module('authorific')
.controller('MainController', function ($scope, $timeout, $state, $stateParams, $window, $rootScope, $cookies, $mdSidenav, authorificBackend, Global) {
  'use strict';

  //Set user data
  $scope.user = Global.user;
  $scope.userAuthenticated = Global.authenticated;

  authorificBackend.setUser(user);

  //Fix for the not correct tab being highlighted
  $scope.tabSelected = 0;
  $scope.doNotShowTabState = false;

  $scope.$on('$stateChangeSuccess', function(event, toState, toParams) {
    $timeout(function() {
      $scope.doNotShowState = false;
      if(toState.name === 'home') {
        if(toParams.type === 'popular') {
          $scope.tabSelected = 0;
        }
        else if(toParams.type === 'trending'){
          $scope.tabSelected = 1;
        }
        else if (toParams.type === 'new'){
          $scope.tabSelected = 2;
        }
      }
      else if (toState.name === 'ongoing') {
        $scope.tabSelected = 3;
      }
      else {
        $scope.doNotShowTabState = true;
      }
    });
  });

  $scope.openMenu = function($mdOpenMenu, ev) {
    $mdOpenMenu(ev);
  };

  $scope.showWelcome = true;

  $scope.hideWelcome = function() {
    $scope.showWelcome = false;
  }

  $scope.facebookLogin = function () {
    window.open("/auth/facebook","Login with Facebook", "width=450,height=300,scrollbars=no");
  }

  $scope.facebookLogout = function() {
    authorificBackend.signOut().$promise.then(function(response){
      var cookies = $cookies.getAll();
      angular.forEach(cookies, function (v, k) {
        $cookies.remove(k);
      });
      $window.location.reload();
    });
  };
  
  $scope.openLeftMenu = function() {
    $mdSidenav('left').toggle();
  };

  $scope.closeSideNav = function(){
      $mdSidenav('left').close();
  };

  if($scope.userAuthenticated) {
    authorificBackend.getFacebookUser($scope.user.facebookUserId).$promise.then(function(response) {
      $scope.user.facebookInformation = response;
    });
  }
});
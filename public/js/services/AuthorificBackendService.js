'use strict';

/**
  * @ngdoc function
  * @name teamieApp.factory: authorificBackend
  * @description
  * # authorificBackend
  * Service for accessing Authorific backend servers
  */

angular.module('authorific')

.service('authorificBackend', function ($resource, $http) {

  var authorificBackend = {};

  var storiesUrl = '/stories';

  var storyUrl = '/stories/:storyId';

  var storyLikeUrl = '/stories/:storyId/like';

  var storyShareUrl = '/stories/:storyId/share';

  var storyCommentsUrl = '/stories/:storyId/comments';

  var storyPostUrl = '/stories/:storyId/posts/:postId';

  var facebookUserUrl = '/users/:facebookUserId';

  var signoutUrl = '/signout';

  var facebookUser = $resource(facebookUserUrl, {facebookUserId: '@facebookUserId'}, {
    get: {
      method: 'GET',
      cache: 'true',
      ignoreLoadingBar: true
    }
  });

  var stories = $resource(storiesUrl, {}, {
    get: {
      method: 'GET',
      isArray: true
    },
    post: {
      method: 'POST'
    }
  });

  var story = $resource(storyUrl, {storyId: '@storyId'}, {
    get: {
      method: 'GET',
    },
    post: {
      method: 'POST'
    },
    put: {
      method: 'PUT'
    },
    delete: {
      method: 'DELETE'
    }
  });

  var storyLike = $resource(storyLikeUrl, {storyId: '@storyId'}, {
    put: {
      method: 'PUT'
    }
  });

  var storyShare =  $resource(storyShareUrl, {storyId: '@storyId'}, {
    post: {
      method: 'POST'
    }
  });

  var storyComments =  $resource(storyCommentsUrl, {storyId: '@storyId'}, {
    post: {
      method: 'GET'
    }
  });

  var storyPost = $resource(storyPostUrl, {storyId: '@storyId', postId: '@postId'}, {
    put: {
      method: 'PUT'
    },
    delete: {
      method: 'DELETE'
    }
  });

  var signOut = $resource(signoutUrl, {}, {
    post: {
      method: 'POST'
    }
  });


  authorificBackend.getFacebookUser = function(facebookUserId) {
    return facebookUser.get({facebookUserId: facebookUserId});
  };

  //Ongoing, higher index -> newer stories
  authorificBackend.getStories = function(type, offset, limit) {
    return stories.get({type: type, offset: offset, limit: limit});
  };

  authorificBackend.createStory = function(title, maxPost, isAnonymous, firstPost, imageUrl) {
    if(imageUrl === undefined) {
      return stories.post({
        title: title,
        maxPost: maxPost,
        isAnonymous: isAnonymous,
        firstPost: firstPost,
      });
    }
    else{ 
      return stories.post({
        title: title,
        maxPost: maxPost,
        isAnonymous: isAnonymous,
        firstPost: firstPost,
        imageUrl: imageUrl
      });
    }
  };

  authorificBackend.getStory = function(storyId) {
    return story.get({storyId: storyId});
  };

  authorificBackend.likeStory = function(storyId) {
    return storyLike.put({storyId: storyId});
  };

  authorificBackend.shareStory = function(storyId, message) {
    return storyShare.post({storyId: storyId, message: message});
  }

  authorificBackend.getStoryComments = function(storyId) {
    return storyComments.get({storyId: storyId});
  }

  //Post starts with id = 2, upon being created, first post has an id of 1
  authorificBackend.createPost = function(storyId, content, postNumber, isAnonymous) {
    return story.post({
      storyId: storyId,
      content: content,
      postNumber: postNumber,
      isAnonymous: isAnonymous
    });
  };

  authorificBackend.finishStory = function(storyId) {
    return story.put({
      storyId: storyId
    });
  };

  authorificBackend.deleteStory = function(storyId) {
    return story.delete({
      storyId: storyId
    });
  };

  authorificBackend.updatePost = function(storyId, content, postId) {
    return storyPost.put({
      storyId: storyId,
      postId: postId,
      content: content
    });
  };

  authorificBackend.deletePost = function(storyId, postId) {
    return storyPost.delete({
      storyId: storyId,
      postId: postId
    });
  };

  authorificBackend.signOut = function() {
    return signOut.post();
  }

  authorificBackend.setUser = function(user) {
    authorificBackend.user = user;
  }

  authorificBackend.getUser = function() {
    return authorificBackend.user;
  }

  return authorificBackend;

});
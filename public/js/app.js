angular
  .module('authorific', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ui.router',
    'ngMaterial',
    'facebook',
    'authorific.system',
    'infinite-scroll',
    'angular-loading-bar',
    'angular-img-cropper',
    'ngImgur',
    'angular-svg-round-progress'
  ])
  .run(function ($rootScope, $state, $stateParams, $log) {
    //This is to make state and state params accessible everywhere without
    //manually needing to add it
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.log = $log.log;
  })
  .config(function(FacebookProvider) {
     FacebookProvider.init('669717136462732');
  })
  .config(function ($stateProvider, $urlRouterProvider) {
    //If unrecognized, redirect to home
    /*
    Activate this when we want to redirect users to base url upon some invalid url entered
    
    Add this snippet to the config ', $urlRouterProvider'
    */
    $urlRouterProvider
    .when('', '/home', '/create', '/login')
    .otherwise('/');
    

    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: '/views/component/home.html',
      controller: 'StoryController',
      params: {type: 'popular'}
    })
    .state('story', {
      url: '/story/{storyId:[0-9]+}',
      templateUrl: '/views/component/story.html',
      controller: 'StoryViewController'
    })
    .state('create', {
      url: '/create',
      templateUrl: '/views/component/create.html',
      controller: 'CreateController'
    })
    .state('contribute', {
      url: '/contribute',
      templateUrl: '/views/component/contribute.html',
      controller: 'ContributeController'
    })
    .state('login', {
      url: '/login',
      templateUrl: '/views/state/login.html'
    })
    .state('ongoing', {
      url: '/ongoing',
      templateUrl: '/views/component/ongoing.html',
      controller: 'OngoingController'
    })
    .state('library', {
      url: '/library',
      templateUrl: '/views/component/library.html',
      controller: 'LibraryController'
    })
    .state('policy', {
      url: '/policy',
      templateUrl: '/views/component/policy.html'
    })
    .state('about', {
      url: '/about',
      templateUrl: '/views/component/about.html'
    });
  })

/*
  .config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('grey', {
        'hue-1': '50',
        'hue-2': '100',
        'hue-3': '200'
      });
  })
*/

  .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 20;
  }]);

  angular.module('authorific.system',[]);

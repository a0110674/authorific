module.exports = {
    // This is your MYSQL Database configuration
    db: {
        name: "mean_relational",
        password: "",
        username: "root",
        host:"localhost",
        port:3306
    },
    app: {
        name: "MEAN - A Modern Stack - Production"
    },
    facebook: {
        clientID: "APP_ID",
        clientSecret: "APP_SECRET",
        callbackURL: "http://localhost:3000/auth/facebook/callback",
        clientToken: "APP_ACCESS_TOKEN",
        pageID: "PAGE_ID",
        pageToken: "PAGE_ACCESS_TOKEN"
    },
    twitter: {
        clientID: "CONSUMER_KEY",
        clientSecret: "CONSUMER_SECRET",
        callbackURL: "http://localhost:3000/auth/twitter/callback"
    },
    github: {
        clientID: "APP_ID",
        clientSecret: "APP_SECRET",
        callbackURL: "http://localhost:3000/auth/github/callback"
    },
    google: {
        clientID: "APP_ID",
        clientSecret: "APP_SECRET",
        callbackURL: "http://localhost:3000/auth/google/callback"
    }
};
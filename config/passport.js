
var passport = require('passport');
// These are different types of authentication strategies that can be used with Passport.
var FacebookStrategy = require('passport-facebook').Strategy;
var config = require('./config');
var db = require('./sequelize');
var winston = require('./winston');

//Serialize sessions
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    db.User.find({where: {id: id}}).then(function(user){
        winston.info('Session: { id: ' + user.id + ' }');
        done(null, user);
    }).catch(function(err){
        done(err, null);
    });
});


// Use facebook strategy
passport.use(new FacebookStrategy({
        clientID: config.facebook.clientID,
        clientSecret: config.facebook.clientSecret,
        callbackURL: config.facebook.callbackURL
    },
    function(accessToken, refreshToken, profile, done) {
        db.User.find({where : {facebookUserId: profile.id}}).then(function(user){
            if(!user){
                db.User.create({
                    facebookUserId: profile.id,
                    accessToken: accessToken
                }).then(function(u){
                    winston.info('New User (facebook) : { id: ' + u.id + ' }');
                    return done(null, u);
                })
            } else {
                winston.info('Login (facebook) : { id: ' + user.id + ' }');
                user.updateAttributes({
                    accessToken: accessToken
                });
                return done(null, user);
            }
        }).catch(function(err){
            done(err, null);
        });
    }
));

module.exports = passport;

